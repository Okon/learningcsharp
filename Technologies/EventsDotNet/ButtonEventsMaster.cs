﻿using System;

namespace EventsDotNet
{
    public class ButtonEventsMaster
    {
        public EventHandler<ButtonPressedEventArg>? ButtonPressed;

        public void OnButtonPressed(char keyCode)
        {
            ButtonPressed?.Invoke(this, new ButtonPressedEventArg(keyCode));
        }

    }
}