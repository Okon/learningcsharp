﻿namespace EventsDotNet
{
    public class ButtonPressedEventArg
    {
        public ButtonPressedEventArg(char keyCode)
        {
            KeyCode = keyCode;
        }

        public char KeyCode { get; }
    }
}