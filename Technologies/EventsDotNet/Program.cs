﻿using System;
using EventsDotNet;

//IT is example of rather obsolete approach with manual handling event with built in .NET System;
// .NET 6.0 

var keyCode = 'a';
var buttonMaster = new ButtonEventsMaster();
CreateButtonPressedEvent(buttonMaster);
CreateSecondButtonPressedEvent(buttonMaster);

while (keyCode != 'x')
{
    Console.WriteLine("Press X to exit");
    keyCode = Console.ReadKey(true).KeyChar;
    buttonMaster.OnButtonPressed(keyCode);
}

void CreateButtonPressedEvent(ButtonEventsMaster buttonEventsMaster)
{
    buttonEventsMaster.ButtonPressed += (sender, eventArgs) =>
    {
        Console.WriteLine($@"Pressed button {eventArgs.KeyCode}");
    };
}

void CreateSecondButtonPressedEvent(ButtonEventsMaster buttonEventsMaster)
{
    buttonEventsMaster.ButtonPressed += (sender, eventArgs) =>
    {
        Console.WriteLine($@"Pressed button {eventArgs.KeyCode} from different event");
    };
}