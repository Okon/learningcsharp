﻿namespace Ordering.Domain.Common;

public abstract class EntityBase : IEntityBase
{
    public Guid Id { get; protected set; }
    public Guid CreatedBy { get; protected set; }
    public DateTime CreatedDate { get; protected set; }
    public Guid LastModifiedBy { get; protected set; }
    public DateTime? LastModificationDate { get; protected set; }
}