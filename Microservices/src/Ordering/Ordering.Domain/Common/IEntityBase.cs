﻿namespace Ordering.Domain.Common;

public interface IEntityBase
{
    Guid Id { get; }
    Guid CreatedBy { get; }
    DateTime CreatedDate { get; }
    Guid LastModifiedBy { get; }
    DateTime? LastModificationDate { get; }
}