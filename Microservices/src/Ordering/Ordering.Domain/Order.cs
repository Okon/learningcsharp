﻿using System.Security.Principal;
using Ordering.Domain.Common;

namespace Ordering.Domain;

public class Order : EntityBase
{
    public float Amount { get; private set; }
    public float TotalPrice { get; private set; }
}