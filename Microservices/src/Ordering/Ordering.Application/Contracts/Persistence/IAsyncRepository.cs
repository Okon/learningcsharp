﻿using System.Linq.Expressions;
using Ordering.Domain.Common;

namespace Ordering.Application.Contracts.Persistence;

public interface IAsyncRepository<TEntity> where TEntity : EntityBase
{
    Task<IReadOnlyCollection<TResponse>> GetAllAsync<TResponse>() where TResponse : IResponse;
    Task<IReadOnlyCollection<TEntity>> GetAllAsync();
    Task<IReadOnlyCollection<TResponse>> GetAsync<TResponse>(Expression<Func<TEntity, bool>> predicate) where TResponse : IResponse;
    Task<IReadOnlyCollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate);
    Task AddAsync(TEntity entity);
    Task UpdateAsync(TEntity entity);
    Task DeleteAsync(TEntity entity);
}