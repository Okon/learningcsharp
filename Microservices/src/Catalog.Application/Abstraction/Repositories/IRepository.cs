﻿namespace Catalog.Application.Abstraction.Repositories;

public interface IRepository<T> where T : class
{
    IQueryable<T> GetAll();
    Task<T> GetById(int id);
    Task Add(T entity);
    Task Update(T entity);
    Task Delete(T entity);
    Task Delete(int id);
}